<?php

namespace DynDns\DynDnsBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DynDnsBundle
 * @package DynDns\DynDnsBundle
 */
class DynDnsBundle extends Bundle
{
    const MAJOR_VERSION = 1;
    const MINOR_VERSION = 0;
    const REVISION_VERSION = 0;

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}
