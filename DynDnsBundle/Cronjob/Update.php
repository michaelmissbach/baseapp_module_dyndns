<?php

namespace DynDns\DynDnsBundle\Cronjob;

use BaseApp\BaseappBundle\Interfaces\IAppSettingGroups;
use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Service\SettingsService;
use BaseApp\BaseappBundle\Cronjob\ICronjob;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class Update
 * @package DynDns\DynDnsBundle\Cronjob
 */
class Update implements ICronjob, IAppSettingGroups, IAppSettings
{
    /**
     * @param ParameterBag $request
     * @param ParameterBag $parameterBag
     * @throws \Exception
     */
    public function run(ParameterBag $request, ParameterBag $parameterBag): void
    {
        $user = SettingsService::$instance->get(SettingsService::createSemanticKey('dyn_dns','dyn_dns_user'));
        $pass = SettingsService::$instance->get(SettingsService::createSemanticKey('dyn_dns','dyn_dns_password'));
        $hostname = SettingsService::$instance->get(SettingsService::createSemanticKey('dyn_dns','dyn_dns_host'));

        //IPV4:
        $currentIp = file_get_contents("http://ipecho.net/plain");
        //IPV6
        //$currentIp = json_decode(file_get_contents("http://api6.ipify.org/?format=json"),true)['ip'];

        $updateUrlPattern = 'https://%s:%s@update.spdyn.de/nic/update?hostname=%s&myip=%s';
        $checkIp = $parameterBag->get('ip');

        if (trim($checkIp) != trim($currentIp)) {
            $url = sprintf($updateUrlPattern,$user,$pass,$hostname,$currentIp);
            echo $url.PHP_EOL;
            echo file_get_contents($url).PHP_EOL;
            $parameterBag->set('ip',$currentIp);            
            echo sprintf('Update ip to %s.%s',$currentIp,PHP_EOL);
        }
    }

    /**
     * @return int
     */
    public function getSecondInterval(): int
    {
        return (int)SettingsService::$instance->get(SettingsService::createSemanticKey('dyn_dns','dyn_dns_interval'),300);
    }

    public function getAppSettingGroups(): array
    {
        return [
            [
                SettingsService::SETTINGS_GROUP_KEY => 'dyn_dns',
                SettingsService::SETTINGS_GROUP_NAME => 'Dyn Dns'
            ]
        ];
    }

    public function getAppSettings(): array
    {
        return [
            [
                SettingsService::SETTINGS_KEY => 'dyn_dns_interval',
                SettingsService::SETTINGS_GROUP_KEY => 'dyn_dns',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Interval'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ],
            [
                SettingsService::SETTINGS_KEY => 'dyn_dns_user',
                SettingsService::SETTINGS_GROUP_KEY => 'dyn_dns',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'User'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ],
            [
                SettingsService::SETTINGS_KEY => 'dyn_dns_password',
                SettingsService::SETTINGS_GROUP_KEY => 'dyn_dns',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Password'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ],
            [
                SettingsService::SETTINGS_KEY => 'dyn_dns_host',
                SettingsService::SETTINGS_GROUP_KEY => 'dyn_dns',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Hostname'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ]
        ];
    }
}